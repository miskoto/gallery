$(document).ready(function () {
    $(".gallery").fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        autoSize: false,
        minWidth: 100,
        minHeight: 100,
        width: 100,
        height: 100,
        scrolling: 'yes'
    });

});
